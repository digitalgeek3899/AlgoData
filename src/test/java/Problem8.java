public class Problem8 {

    public static boolean isAllDigits(String str, int index) {
        // Base cases
        if (index == str.length()) {
            return true; // Reached end of string, all characters processed
        } else if (!Character.isDigit(str.charAt(index))) {
            return false; // Encountered a non-digit character
        }

        // Recursive case: Check next character
        return isAllDigits(str, index + 1);
    }

    public static boolean isAllDigits(String str) {
        if (str == null || str.isEmpty()) {
            return false; // Empty string doesn't consist only of digits
        }
        return isAllDigits(str, 0); // Start checking from the beginning
    }

    public static void main(String[] args) {
        String str1 = "12345";
        String str2 = "hello";
        String str3 = "";

        System.out.println(str1 + " consists only of digits: " + isAllDigits(str1));
        System.out.println(str2 + " consists only of digits: " + isAllDigits(str2));
        System.out.println(str3 + " consists only of digits: " + isAllDigits(str3));
    }
}