
import java.util.Scanner;

public class Problem2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        double average = calculateAverage(arr); // Get the average using the new function
        System.out.println("The average of the array elements is: " + average);
    }

    // Function to calculate the average of an array
    public static double calculateAverage(int[] arr) {
        // Check for empty array and handle appropriately (here, returning Double.NaN)
        if (arr.length == 0) {
            return Double.NaN;
        }

        // Calculate the sum of all elements
        double sum = 0;
        for (int num : arr) {
            sum += num;
        }

        // Calculate and return average (using double for decimal possibility)
        return sum / arr.length;
    }
}

