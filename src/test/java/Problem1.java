import java.util.Scanner;

public class Problem1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        int minimum = findMinimum(arr); // Get the minimum value using the new function
        System.out.println("The minimum element in the array is: " + minimum);
    }

    // Function to find the minimum element in an array
    public static int findMinimum(int[] arr) {
        int min = arr[0]; // Assume the first element is the minimum initially
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i]; // Update the minimum if a smaller element is found
            }
        }
        return min;
    }
}
