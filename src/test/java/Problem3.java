import java.util.Scanner;

public class Problem3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter a number: ");
        int num = sc.nextInt();

        if (isPrime(num)) {
            System.out.println(num + " is a prime number");
        } else {
            System.out.println(num + " is not a prime number");
        }
    }

    public static boolean isPrime(int n, int i) {
        // Base cases
        if (n <= 1) {
            return false; // 1 or less are not prime
        } else if (n <= 3) {
            return true; // 2 and 3 are prime
        } else if (n % 2 == 0 || n % 3 == 0) {
            return false; // Divisible by 2 or 3, not prime (except 2 and 3)
        }

        // Check divisibility by numbers of the form 6k ± 1 (faster than all numbers)
        while (i * i <= n) {
            if (n % (i) == 0 || n % (i + 2) == 0) {
                return false;
            }
            i += 6;
        }

        return true; // No divisors found, n is prime
    }

    public static boolean isPrime(int n) {
        return isPrime(n, 5); // Start checking for divisibility from 5
    }


}

