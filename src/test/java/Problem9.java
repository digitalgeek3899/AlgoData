public class Problem9 {

    public static int binomialCoefficient(int n, int k) {
        // Base cases
        if (k == 0 || k == n) {
            return 1;
        } else if (k < 0 || k > n) {
            return 0; // Handle invalid k values (negative or greater than n)
        } else {
            // Recursive case: C(n, k) = C(n-1, k-1) + C(n-1, k)
            return binomialCoefficient(n - 1, k - 1) + binomialCoefficient(n - 1, k);
        }
    }

    public static void main(String[] args) {
        int n = 5;
        int k = 2;
        int coefficient = binomialCoefficient(n, k);
        System.out.println("C(" + n + ", " + k + ") = " + coefficient);
    }
}
