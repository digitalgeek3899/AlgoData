public class Problem5 {

    public static int fibonacci(int n) {
        // Base cases
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else {
            // Recursive case: Fn = Fn-1 + Fn-2
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }

    public static void main(String[] args) {
        int num = 10; // Enter a non-negative number here
        int fibValue = fibonacci(num);
        System.out.println("The " + num + "th element in the Fibonacci sequence is: " + fibValue);
    }
}
