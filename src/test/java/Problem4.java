public class Problem4 {

    public static int factorial(int n) {
        // Base case: factorial of 0 is 1
        if (n == 0) {
            return 1;
        } else {
            // Recursive case: n! = n * (n-1)!
            return n * factorial(n - 1);
        }
    }

    public static void main(String[] args) {
        int num = 5; // Enter a non-negative number here
        int result = factorial(num);
        System.out.println("The factorial of " + num + " is: " + result);
    }
}
